

# Proyecto con Node, MongoDB y Docker
Proyecto de API REST para agregar, editar y obtener recetas. 
## Requisitos

- [Docker](https://docs.docker.com/engine/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)
## Configuración

1. Clona este repositorio en tu computadora:

```
git clone https://gitlab.com/amarininsfran/recetario.git
```

2. Cambia al directorio del proyecto:

```
cd Recetario/
```

## Uso con Docker

1. Inicia la aplicación con Docker:

```
docker compose up
```

La aplicación estará disponible en `http://localhost:3000`.
